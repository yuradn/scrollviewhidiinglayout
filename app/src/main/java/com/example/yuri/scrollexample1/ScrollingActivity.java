package com.example.yuri.scrollexample1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ScrollingActivity extends AppCompatActivity {

    private final static String TAG = "ScrollingActivity";
    @Bind(R.id.llSettings)
    LinearLayout llSettings;
    @Bind(R.id.tvFromTo)
    TextView tvFromTo;
    boolean isDrop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDrop) return;
                Snackbar.make(view, "Settings: ebabled.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                isDrop=false;
            }
        });

        final NestedScrollView nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Log.d(TAG, "scrollX: " + scrollX
                        + " scrollY: " + scrollY
                        + " oldScrollX: " + oldScrollX
                        + " oldScrollY: " + oldScrollY);
                //if (tvFromTo!=null) Log.d(TAG, "TextView X: " + tvFromTo.getX() +" Y: " + tvFromTo.getY());
                if (scrollY>tvFromTo.getY()) {
                    isDrop=true;
                }
                if (isDrop) {
                    if (scrollY<tvFromTo.getY())
                    //if (oldScrollY<scrollY) return;
                    nestedScrollView.setScrollY((int)tvFromTo.getY());
                    //nestedScrollView.setScaleY(tvFromTo.getY());
                    //nestedScrollView.setTranslationY(tvFromTo.getY());
                    //nestedScrollView.setY(tvFromTo.getY());
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
